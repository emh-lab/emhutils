#' Title
#'
#' @param df_sacrois a sacrois data frame coming from sacrois_jour_flux_load.R
#' @param origin Origin of the quantitave variable : SIPA or SACROIS or MOYENNE
#' @param var_fishing_time variable to use as the fishing time TP_NAVIRE or TPS_MER
#' @param var_weight variable to use as the weight time QUANT_POIDS_VIF as default
#' @param var_euro variable to use as the values time MONTANT_EUROS as default
#' @param country select landings per country if NULL all country by default FRA only
#' @param reduce_col select a reduced number of columns, drop columns that are note usefull
#' @param save_df if TRUE save data frame
#' @param path_data_sacrois if save_df is TRUE, specify the path to save the df
#' @param filter_na if TRUE remove rows with NA for fishing_time, weight and euro
#' @param erase_rds if true erase previously saved rds files, if not rds files is loaded from previous run
#'
#' @return a sacrois data frame
#' @export
#'
#' @examples df_sacrois <- sacrois_jour_flux_load(sacrois_data_raw = sacrois_raw) %>%
#' sacrois_set_origin(.)
sacrois_set_origin <- function(df_sacrois,
                               origin = "SACROIS",
                               var_fishing_time = "TP_NAVIRE",
                               var_weight = "QUANT_POIDS_VIF",
                               var_euro = "MONTANT_EUROS",
                               country = "FRA",
                               reduce_col = TRUE,
                               save_df = FALSE,
                               erase_rds = TRUE,
                               path_data_sacrois = NULL,
                               filter_na = FALSE) {
  ### ---------------------------------------------------------------------------
  ### check if file exists in case when erase_rds == FALSE
  if ((erase_rds == FALSE & filter_na == FALSE &
       file.exists(paste0(
         path_data_sacrois,
         "/df_sacrois_origin_", unique(df_sacrois$YEAR), ".rds"
       ))) |

      (erase_rds == FALSE & filter_na == TRUE &
       file.exists(paste0(
         path_data_sacrois,
         "/df_sacrois_no_na_origin_", unique(df_sacrois$YEAR), ".rds"
       )))) {
    if (erase_rds == FALSE & filter_na == FALSE &
        file.exists(paste0(
          path_data_sacrois,
          "/df_sacrois_origin_", unique(df_sacrois$YEAR), ".rds"
        ))) {
      df_sacrois <- readr::read_rds(file = paste0(
        path_data_sacrois,
        "/df_sacrois_origin_", unique(df_sacrois$YEAR), ".rds"
      ))
    }

    if (erase_rds == FALSE & filter_na == TRUE &
        file.exists(paste0(
          path_data_sacrois,
          "/df_sacrois_no_na_origin_", unique(df_sacrois$YEAR), ".rds"
        ))) {
      df_sacrois <- readr::read_rds(file = paste0(
        path_data_sacrois,
        "/df_sacrois_no_na_origin_", unique(df_sacrois$YEAR), ".rds"
      ))
    }
  } else {
    ### ---------------------------------------------------------------------------
    ### Set variable of interest
    var_fishing_time_origin <- paste(var_fishing_time, origin, sep = "_")
    var_weight_origin <- paste(var_weight, origin, sep = "_")
    var_euro_origin <- paste(var_euro, origin, sep = "_")

    ### ---------------------------------------------------------------------------
    ### check if they exists
    stopifnot(
      var_fishing_time_origin %in% names(df_sacrois),
      var_weight_origin %in% names(df_sacrois),
      var_euro_origin %in% names(df_sacrois)
    )
    ### ---------------------------------------------------------------------------
    if (reduce_col) {
      # Les variables qui nous interessent
      selected_col <- c(
        "NAVS_COD", # Immatriculation du navire
        "MAREE_ID", # Identifiant de la maree Sacrois
        "MAREE_DATE_DEP", # Date de départ de la marée
        "MAREE_DATE_RET", # Date de retour de la marée
        "TPS_MER", # Temps passé en mer
        "LIEU_COD_DEP_SACROIS", # Lieu de départ consolidé
        "LIEU_COD_RET_SACROIS", # Lieu de retour consolidé
        "SEQ_ID", # Séquence de pêche
        "DATE_SEQ", # Date séquence de pêche
        "METIER_COD_SACROIS", # Métier de la marée
        "METIER_DCF_6_COD", # Métier niveau 6
        "ENGIN_COD", # Engin de pêche
        "SECT_COD_SACROIS_NIV3", # Zone niveau 3
        "SECT_COD_SACROIS_NIV5", # Zone niveau 5
        "ESP_COD_FAO", # Espece pêchée
        "DIMENSION", # Dimension de l'engin
        "MAILLAGE", # Maillage de l'engin
        "MAILLAGE_ORIGINE", # Origine du maillage de l'engin
        "YEAR",
        "MONTH",
        "QUARTER"
      )
      selected_col <- c(
        selected_col,
        var_weight_origin,
        var_fishing_time_origin,
        var_euro_origin
      )
    } else {
      selected_col <- names(df_sacrois)
    }

    ### ---------------------------------------------------------------------------
    ### mutate data
    df_sacrois <- df_sacrois %>%
      purrr::when(
        is.null(country) ~ .,
        ~ dplyr::filter(., .data$PAVILLON %in% country)
      ) %>%
      dplyr::select(dplyr::all_of(selected_col)) %>%
      dplyr::mutate(
        fishing_time = .data[[var_fishing_time_origin]],
        weight = .data[[var_weight_origin]],
        euro = .data[[var_euro_origin]],
        origin = origin,
        LE_ID_JOUR = factor(paste(.data$NAVS_COD,
                                  .data$DATE_SEQ,
                                  .data$SECT_COD_SACROIS_NIV5,
                                  .data$ENGIN_COD,
                                  sep = "-"
        )),
        LE_ID_MAREE = factor(paste(.data$NAVS_COD,
                                   .data$MAREE_ID,
                                   .data$MAREE_DATE_DEP,
                                   .data$SECT_COD_SACROIS_NIV5,
                                   .data$ENGIN_COD,
                                   sep = "-"
        )),
        LE_ID_SEQ = factor(paste(.data$LE_ID_MAREE, .data$SEQ_ID, sep = "-")),
        LE_ID_JOUR_ESP = factor(paste(.data$LE_ID_JOUR,
                                      .data$fishing_time,
                                      .data$ESP_COD_FAO, sep = "-")),
        dplyr::across(where(is.factor), forcats::fct_drop)
      ) %>%
      dplyr::select(
        -dplyr::starts_with(var_fishing_time),
        -dplyr::starts_with(var_weight),
        -dplyr::starts_with(var_euro)
      ) %>%
      ### drop NA euros if filter_na == TRUE
      purrr::when(
        filter_na ~ dplyr::filter(
          .,
          !is.na(fishing_time),
          !is.na(TPS_MER),
          !is.na(weight),
          !is.na(euro)
        ),
        ~.
      ) %>%
      ### keep only one duplicated rows if any
      dplyr::arrange(.data$LE_ID_JOUR_ESP, dplyr::desc(.data$weight)) %>%
      dplyr::group_by(.data$LE_ID_JOUR_ESP) %>%
      dplyr::slice(1) %>%
      dplyr::ungroup() %>%
      dplyr::mutate(dplyr::across(where(is.factor), forcats::fct_drop)) %>%
      dplyr::select(-"LE_ID_JOUR_ESP")


    ### ---------------------------------------------------------------------------
    ### save data if save_df == TRUE
    if (save_df) {
      if (is.null(path_data_sacrois)) {
        path_data_sacrois <- here::here()
      }
      if (filter_na) {
        saveRDS(df_sacrois,
                file = paste0(paste0(
                  path_data_sacrois,
                  "/df_sacrois_no_na_origin_",
                  origin, "_",
                  unique(df_sacrois$YEAR), ".rds"
                ))
        )
      } else {
        saveRDS(df_sacrois,
                file = paste0(paste0(
                  path_data_sacrois,
                  "/df_sacrois_origin_",
                  origin, "_",
                  unique(df_sacrois$YEAR), ".rds"
                ))
        )
      }
    }
  }
  ### ---------------------------------------------------------------------------
  return(df_sacrois)
}


#' drop columns of a data frame if all elements in that column is NA
#'
#' @param df a data frame
#'
#' @return a data frame witout columns full of NA
#' @export
#'
#' @examples
#' df_test <- data.frame(
#'   x1 = rnorm(10),
#'   x2 =  rnorm(10),
#'   x3 = NA
#' )
#' drop_NA_cols(df_test)
drop_NA_cols <- function(df) {
  df %>%
    dplyr::select(
      where(
        ~ !all(is.na(.x))
      )
    )
}

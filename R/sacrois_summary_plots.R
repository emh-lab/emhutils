#' Make plot to summarize sacrois data
#'
#' @param df_sacrois a sacrois data frame
#' @param gear_var gear variable to compute gear proportion per year
#' @param species_cod_fao a vector of one or more species FAO code
#' @param data_origin character string stating the origin of the data to add in produced data frame
#' @param thr_avg_landings_rect_stat threshold for plotting average yearly landings per rect stat
#' @param keep_df if TRUE keep data.frame used to make plots in outputs list
#'
#' @return a list of summary data frame and plots
#' @export
#'
#' @examples
#' dfs_sacrois <-  sacrois_jour_flux_load(sacrois_data_raw = sacrois_raw) %>%
#' sacrois_set_origin(.) %>%
#' sacrois_set_gear(.,
#' gear_var = "METIER_DCF_6_COD",
#' remove_metier_var = FALSE) %>%
#' sacrois_summary_plots(.)
#' dfs_sacrois_sole <-  sacrois_jour_flux_load(sacrois_data_raw = sacrois_raw) %>%
#' sacrois_set_origin(.) %>%
#' sacrois_set_gear(.,
#' gear_var = "METIER_DCF_6_COD",
#' remove_metier_var = FALSE) %>%
#' sacrois_summary_plots(., species_cod_fao = "SOL")
sacrois_summary_plots <- function(df_sacrois,
                                  gear_var = "gear",
                                  species_cod_fao = NULL,
                                  data_origin = NULL,
                                  thr_avg_landings_rect_stat = NULL,
                                  keep_df = FALSE){

  stopifnot(any(gear_var %in% names(df_sacrois)))
  ###---------------------------------------------------------------------------
  list_summary_plot <- list()

  ###---------------------------------------------------------------------------
  if (!is.null(species_cod_fao)) {

    stopifnot(any(species_cod_fao %in% df_sacrois$ESP_COD_FAO))

    df_sacrois <- df_sacrois %>%
      dplyr::filter(.data$ESP_COD_FAO %in% species_cod_fao) %>%
      dplyr::mutate(dplyr::across(where(is.factor), forcats::fct_drop))

    dfs_sacrois_landings_year <- df_sacrois %>%
      dplyr::group_by(.data$YEAR, .data$ESP_COD_FAO) %>%
      dplyr::summarise(weight_sum =  sum(.data$weight),
                       weight_sum_t = .data$weight_sum/1000) %>%
      purrr::when(is.null(data_origin) ~ .,
                  ~ mutate(., data_origin = data_origin))

    if (keep_df == TRUE) {
      list_summary_plot$dfs_sacrois_landings_year <- dfs_sacrois_landings_year
    }

    list_summary_plot$plot_sacrois_landings_year <- ggplot2::ggplot() +
      ggplot2::geom_col(data = dfs_sacrois_landings_year,
                        ggplot2::aes(x = .data$YEAR, y = .data$weight_sum_t)) +
      ggplot2::xlab("Year") +
      ggplot2::ylab("Landings (in t)") +
      ggplot2::ggtitle("Total landings (in t) per year") +
      ggplot2::theme(axis.text.x =  ggplot2::element_text(angle = 90,
                                                          vjust = 0.5,
                                                          hjust = 1))

    if (length(species_cod_fao) > 1) {
      list_summary_plot$plot_sacrois_landings_year <- list_summary_plot$plot_sacrois_landings_year +
        ggplot2::facet_grid( ~ .data$ESP_COD_FAO)

    }

    dfs_sacrois_landings_month <- df_sacrois %>%
      dplyr::group_by(.data$YEAR, .data$MONTH, .data$ESP_COD_FAO) %>%
      dplyr::summarise(weight_sum = sum(.data$weight),
                       weight_sum_t = .data$weight_sum/1000) %>%
      purrr::when(is.null(data_origin) ~ .,
                  ~ mutate(., data_origin = data_origin))

    if (keep_df == TRUE) {
      list_summary_plot$dfs_sacrois_landings_month <- dfs_sacrois_landings_month
    }


    list_summary_plot$plot_sacrois_landings_month <- ggplot2::ggplot(
      data = dfs_sacrois_landings_month,
      ggplot2::aes(x = .data$MONTH, y = .data$weight_sum_t)) +
      ggplot2::geom_boxplot(outlier.shape = NA)  +
      ggplot2::geom_jitter(shape = 17,
                           alpha = 0.8,
                           position = ggplot2::position_jitter(0.2),
                           ggplot2::aes(color = .data$YEAR)) +
      ggplot2::xlab("Month") +
      ggplot2::ylab("Landings (in t)")

    if (length(species_cod_fao) > 1) {
      list_summary_plot$plot_sacrois_landings_month <- list_summary_plot$plot_sacrois_landings_month +
        ggplot2::facet_grid( ~ .data$ESP_COD_FAO)
    }

    dfs_sacrois_vessels_landings <- df_sacrois %>%
      dplyr::group_by(.data$YEAR, .data$ESP_COD_FAO, .data$NAVS_COD) %>%
      dplyr::summarise(weight_sum =  sum(.data$weight),
                       weight_sum_t = .data$weight_sum/1000) %>%
      dplyr::group_by(.data$ESP_COD_FAO, .data$NAVS_COD) %>%
      dplyr::summarise(weight_average_year = mean(.data$weight_sum),
                       weight_average_year_t = mean(.data$weight_sum_t)) %>%
      purrr::when(is.null(data_origin) ~ .,
                  ~ mutate(., data_origin = data_origin))

    if (keep_df == TRUE) {
      list_summary_plot$dfs_sacrois_vessels_landings <- dfs_sacrois_vessels_landings
    }

    list_summary_plot$plot_sacrois_vessels_landings <- ggplot2::ggplot() +
      ggplot2::geom_histogram(data = dfs_sacrois_vessels_landings,
                              ggplot2::aes(.data$weight_average_year),
                              binwidth = 100,
                              color = "black",
                              fill = "white") +
      ggplot2::ylab("Number of vessels") +
      ggplot2::xlab("Landings (in kg)") +
      ggplot2::ggtitle("Number of vessels and yearly average total landings (in kg)") +
      ggplot2::theme(axis.text.x =  ggplot2::element_text(angle = 90,
                                                          vjust = 0.5,
                                                          hjust = 1))

    if (length(species_cod_fao) > 1) {
      list_summary_plot$plot_sacrois_vessels_landings <- list_summary_plot$plot_sacrois_vessels_landings +
        ggplot2::facet_grid( ~ .data$ESP_COD_FAO)

    }

    ###---------------------------------------------------------------------------
    ### Landings per rect-stat
    dfs_sacrois_landings_year_rectstat <- df_sacrois %>%
      dplyr::group_by(.data$YEAR, .data$ESP_COD_FAO, .data$SECT_COD_SACROIS_NIV5) %>%
      dplyr::summarise(weight_sum =  sum(.data$weight),
                       weight_sum_t = .data$weight_sum/1000) %>%
      purrr::when(is.null(data_origin) ~ .,
                  ~ mutate(., data_origin = data_origin))


    if (keep_df == TRUE) {
      list_summary_plot$dfs_sacrois_landings_year_rectstat <- dfs_sacrois_landings_year_rectstat
    }


    list_summary_plot$plot_sacrois_landings_year_rec <- ggplot2::ggplot() +
      ggplot2::geom_col(data = dfs_sacrois_landings_year_rectstat,
                        ggplot2::aes(x = .data$YEAR, y = .data$weight_sum_t)) +
      ggplot2::xlab("Year") +
      ggplot2::ylab("Landings (in t)") +
      ggplot2::ggtitle("Total landings (in t) per year and ICES rectangles") +
      ggplot2::theme(axis.text.x =  ggplot2::element_text(angle = 90,
                                                          vjust = 0.5,
                                                          hjust = 1)) +
      ggplot2::facet_grid(~.data$SECT_COD_SACROIS_NIV5) +
      ggplot2::geom_hline(yintercept = thr_avg_landings_rect_stat)

    if (length(species_cod_fao) > 1) {
      list_summary_plot$plot_sacrois_landings_year_rec <- list_summary_plot$plot_sacrois_landings_year_rec +
        ggplot2::facet_grid(.data$ESP_COD_FAO ~ .data$SECT_COD_SACROIS_NIV5)

    }

    dfs_sacrois_landings_rectstat <- df_sacrois %>%
      dplyr::group_by(.data$YEAR, .data$ESP_COD_FAO, .data$SECT_COD_SACROIS_NIV5) %>%
      dplyr::summarise(weight_sum =  sum(.data$weight),
                       weight_sum_t = .data$weight_sum/1000,
                       euro_sum = sum(.data$euro)) %>%
      dplyr::group_by(.data$ESP_COD_FAO, .data$SECT_COD_SACROIS_NIV5) %>%
      dplyr::summarise(average_annual_weight_t = mean(.data$weight_sum_t),
                       average_annual_euro = mean(.data$euro_sum),
                       sd_annual_weight_t = stats::sd(.data$weight_sum_t),
                       sd_annual_euro = stats::sd(.data$euro_sum),
                       cv_annual_weight_t = stats::sd(.data$weight_sum_t)/mean(.data$weight_sum_t),
                       cv_annual_euro = stats::sd(.data$euro_sum)/mean(.data$euro_sum),
                       average_annual_weight_t_max = .data$average_annual_weight_t + .data$sd_annual_weight_t,
                       average_annual_weight_t_min = .data$average_annual_weight_t - .data$sd_annual_weight_t,
      ) %>%
      dplyr::arrange(dplyr::desc(.data$average_annual_weight_t)) %>%
      purrr::when(is.null(data_origin) ~ .,
                  ~ mutate(., data_origin = data_origin))

    if (keep_df == TRUE) {
      list_summary_plot$dfs_sacrois_landings_rectstat <- dfs_sacrois_landings_rectstat
    }

    list_summary_plot$plot_species_landings_rect_stat_hist <- ggplot2::ggplot(data =  dfs_sacrois_landings_rectstat) +
      ggplot2::geom_col(ggplot2::aes(x = forcats::fct_reorder(.data$SECT_COD_SACROIS_NIV5,
                                                              .data$average_annual_weight_t),
                                     y = .data$average_annual_weight_t)) +
      ggplot2::geom_hline(yintercept = thr_avg_landings_rect_stat,
                          linetype = "dashed") +
      ggplot2::xlab("Ices rect stat") +
      ggplot2::ylab(glue::glue("Average yearly landings of {species_cod_fao} (in t)"))


    list_summary_plot$plot_species_landings_rect_stat <- ggplot2::ggplot(data =  dfs_sacrois_landings_rectstat,
                                                                      ggplot2::aes(x = forcats::fct_reorder(.data$SECT_COD_SACROIS_NIV5,
                                                                                                            .data$average_annual_weight_t),
                                                                                   y = .data$average_annual_weight_t)) +
      ggplot2::geom_point() +
      ggplot2::geom_pointrange(ggplot2::aes(ymin = .data$average_annual_weight_t_min,
                                            ymax = .data$average_annual_weight_t_max))  +
      ggplot2::geom_hline(yintercept = thr_avg_landings_rect_stat,
                          linetype = "dashed") +
      ggplot2::xlab("Ices rect stat") +
      ggplot2::ylab(glue::glue("Average yearly landings of {species_cod_fao} (in t)"))

    if (length(species_cod_fao) > 1) {
      list_summary_plot$plot_species_landings_rect_stat <-  list_summary_plot$plot_species_landings_rect_stat +
        ggplot2::facet_grid(.data$ESP_COD_FAO ~ .data$SECT_COD_SACROIS_NIV5)
      list_summary_plot$plot_species_landings_rect_stat_hist <-  list_summary_plot$plot_species_landings_rect_stat_hist +
        ggplot2::facet_grid(.data$ESP_COD_FAO ~ .data$SECT_COD_SACROIS_NIV5)

    }
  }

  ###---------------------------------------------------------------------------
  list_summary_plot$n_vessels <- emhUtils::length_unique(df_sacrois[["NAVS_COD"]])
  list_summary_plot$n_maree <- emhUtils::length_unique(df_sacrois[["MAREE_ID"]])
  list_summary_plot$n_seq <- emhUtils::length_unique(df_sacrois[["LE_ID_SEQ"]])

  ###---------------------------------------------------------------------------
  ### N vessels per years
  dfs_sacrois_nvessels_year <- df_sacrois %>%
    dplyr::select("NAVS_COD", "YEAR") %>%
    dplyr::distinct() %>%
    dplyr::group_by(.data$YEAR) %>%
    dplyr::summarise(n_NAVS_COD =  dplyr::n()) %>%
    purrr::when(is.null(data_origin) ~ .,
                ~ mutate(., data_origin = data_origin))

  if (keep_df == TRUE) {
    list_summary_plot$dfs_sacrois_nvessels_year <- dfs_sacrois_nvessels_year
  }

  list_summary_plot$plot_nvessels_year <- ggplot2::ggplot() +
    ggplot2::geom_col(data = dfs_sacrois_nvessels_year,
                      ggplot2::aes(x = .data$YEAR, y = .data$n_NAVS_COD)) +
    ggplot2::xlab("Year") +
    ggplot2::ylab("Number of vessels") +
    ggplot2::ggtitle("Number of vessels per year") +
    ggplot2::theme(axis.text.x =  ggplot2::element_text(angle = 90,
                                                        vjust = 0.5,
                                                        hjust = 1))


  dfs_sacrois_vessels_nyear <- df_sacrois %>%
    dplyr::group_by(.data$NAVS_COD) %>%
    dplyr::summarise(n_year = dplyr::n_distinct(.data$YEAR)) %>%
    purrr::when(is.null(data_origin) ~ .,
                ~ mutate(., data_origin = data_origin))


  if (keep_df == TRUE) {
    list_summary_plot$dfs_sacrois_vessels_nyear <- dfs_sacrois_vessels_nyear
  }


  list_summary_plot$plot_sacrois_vessels_nyear <- ggplot2::ggplot() +
    ggplot2::geom_histogram(data = dfs_sacrois_vessels_nyear,
                            ggplot2::aes(.data$n_year),
                            binwidth = 1,
                            color = "black",
                            fill = "white") +
    ggplot2::xlab("Number of active years") +
    ggplot2::ylab("Number of vessels") +
    ggplot2::ggtitle("Number of vessels per number of active years") +
    ggplot2::theme(axis.text.x =  ggplot2::element_text(angle = 90,
                                                        vjust = 0.5,
                                                        hjust = 1))


  ###---------------------------------------------------------------------------
  ### N fishing sequences per years
  dfs_sacrois_nseq_year <- df_sacrois %>%
    dplyr::select("SEQ_ID", "YEAR") %>%
    dplyr::distinct() %>%
    dplyr::group_by(.data$YEAR) %>%
    dplyr::summarise(n_SEQ_ID =  dplyr::n()) %>%
    purrr::when(is.null(data_origin) ~ .,
                ~ mutate(., data_origin = data_origin))


  if (keep_df == TRUE) {
    list_summary_plot$dfs_sacrois_nseq_year <- dfs_sacrois_nseq_year
  }

  list_summary_plot$plot_nseq_year <- ggplot2::ggplot() +
    ggplot2::geom_col(data = dfs_sacrois_nseq_year,
                      ggplot2::aes(x = .data$YEAR, y = .data$n_SEQ_ID)) +
    ggplot2::xlab("Year") +
    ggplot2::ylab("Number of fishing sequences") +
    ggplot2::ggtitle("Total number of fishing sequences per year") +
    ggplot2::theme(axis.text.x =  ggplot2::element_text(angle = 90,
                                                        vjust = 0.5,
                                                        hjust = 1))

  ###---------------------------------------------------------------------------
  ### Proportion of gear per years
  dfs_sacrois_nseq_gear_year <- df_sacrois %>%
    dplyr::select("SEQ_ID", "YEAR",  dplyr::all_of(gear_var)) %>%
    dplyr::distinct() %>%
    dplyr::group_by(.data$YEAR, .data[[gear_var]]) %>%
    dplyr::summarise(n_SEQ_ID_gear =  dplyr::n()) %>%
    dplyr::group_by(.data$YEAR) %>%
    dplyr::mutate(n_SEQ_ID = sum(.data$n_SEQ_ID_gear),
                  prop_SEQ_ID_gear = .data$n_SEQ_ID_gear / .data$n_SEQ_ID) %>%
    purrr::when(is.null(data_origin) ~ .,
                ~ mutate(., data_origin = data_origin))

  if (keep_df == TRUE) {
    list_summary_plot$dfs_sacrois_nseq_gear_year <- dfs_sacrois_nseq_gear_year
  }

  list_summary_plot$plot_nseq_gear_year <- ggplot2::ggplot() +
    ggplot2::geom_col(data = dfs_sacrois_nseq_gear_year,
                      ggplot2::aes(x = .data$YEAR,
                                   y = .data$prop_SEQ_ID_gear,
                                   fill = .data[[gear_var]])) +
    ggplot2::xlab("Year") +
    ggplot2::ylab("Proportion of gears") +
    ggplot2::ggtitle("Proportion of gear used in fishing sequence per year") +
    ggplot2::theme(axis.text.x =  ggplot2::element_text(angle = 90,
                                                        vjust = 0.5,
                                                        hjust = 1))

  ###---------------------------------------------------------------------------
  ### N fishing sequences per years
  dfs_sacrois_nmaree_year <- df_sacrois %>%
    dplyr::select("MAREE_ID", "YEAR") %>%
    dplyr::distinct() %>%
    dplyr::group_by(.data$YEAR) %>%
    dplyr::summarise(n_MAREE_ID =  dplyr::n()) %>%
    purrr::when(is.null(data_origin) ~ .,
                ~ mutate(., data_origin = data_origin))

  if (keep_df == TRUE) {
    list_summary_plot$dfs_sacrois_nmaree_year <- dfs_sacrois_nmaree_year
  }

  list_summary_plot$plot_nmaree_year <- ggplot2::ggplot() +
    ggplot2::geom_col(data = dfs_sacrois_nmaree_year,
                      ggplot2::aes(x = .data$YEAR, y = .data$n_MAREE_ID)) +
    ggplot2::xlab("Year") +
    ggplot2::ylab("Number of maree") +
    ggplot2::ggtitle("Total number of maree per year") +
    ggplot2::theme(axis.text.x =  ggplot2::element_text(angle = 90,
                                                        vjust = 0.5,
                                                        hjust = 1))

  ###---------------------------------------------------------------------------
  ### Proportion of gear per years
  dfs_sacrois_nmaree_gear_year <- df_sacrois %>%
    dplyr::select("MAREE_ID", "YEAR",  dplyr::all_of(gear_var)) %>%
    dplyr::distinct() %>%
    dplyr::group_by(.data$YEAR, .data[[gear_var]]) %>%
    dplyr::summarise(n_MAREE_ID_gear =  dplyr::n()) %>%
    dplyr::group_by(.data$YEAR) %>%
    dplyr::mutate(n_MAREE_ID = sum(.data$n_MAREE_ID_gear),
                  prop_MAREE_ID_gear = .data$n_MAREE_ID_gear/.data$n_MAREE_ID) %>%
    purrr::when(is.null(data_origin) ~ .,
                ~ mutate(., data_origin = data_origin))

  if (keep_df == TRUE) {
    list_summary_plot$dfs_sacrois_nmaree_gear_year <- dfs_sacrois_nmaree_gear_year
  }

  list_summary_plot$plot_nmaree_gear_year <- ggplot2::ggplot() +
    ggplot2::geom_col(data = dfs_sacrois_nmaree_gear_year,
                      ggplot2::aes(x = .data$YEAR,
                                   y = .data$prop_MAREE_ID_gear,
                                   fill = .data[[gear_var]])) +
    ggplot2::xlab("Year") +
    ggplot2::ylab("Proportion of gears") +
    ggplot2::ggtitle("Proportion of gear used in maree per year") +
    ggplot2::theme(axis.text.x =  ggplot2::element_text(angle = 90,
                                                        vjust = 0.5,
                                                        hjust = 1))

  return(list_summary_plot)
}


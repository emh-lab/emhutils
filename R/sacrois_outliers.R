#' Remove outliers from non species variable in a sacrois data frame
#'
#' @param df_sacrois a sacrois data frame coming from sacrois_jour_flux_load.R
#' @param variable which variable to filter from outliers
#' @param min_threshold value for a threshold to remove variable values under that threshold
#' @param max_threshold value for a threshold to remove variable values above that threshold
#' @param sd_threshold remove values that are above : mean(variable, na.rm = TRUE) + sd(variable, na.rm = TRUE) * sd_threshold)
#' @param grouping_variable choose which variable to use in a group_by
#'
#' @return a sacrois data frame with vessels variables
#' @export
#'
#' @examples df_sacrois <-  sacrois_jour_flux_load(sacrois_data_raw = sacrois_raw) %>%
#' sacrois_set_origin(.) %>%
#' sacrois_outliers_sequence_variable(.,
#' variable = "fishing_time",
#' grouping_variable = "METIER_DCF_6_COD",
#' min_threshold = 1)
sacrois_outliers_sequence_variable <- function(df_sacrois,
                                               variable = "fishing_time",
                                               grouping_variable = NULL,
                                               min_threshold = NULL,
                                               max_threshold = NULL,
                                               sd_threshold = NULL) {

  stopifnot(variable %in% colnames(df_sacrois))

  ### --------------------------------------------------------------------------
  ### apply all filter
  df_sacrois <- df_sacrois %>%
    purrr::when(
      is.null(grouping_variable) ~ .,
      ~ dplyr::group_by(., across(all_of(grouping_variable)))
    ) %>%
    purrr::when(
      is.null(min_threshold) ~ .,
      ~ dplyr::filter(., .data[[variable]] > min_threshold)
    ) %>%
    purrr::when(
      is.null(max_threshold) ~ .,
      ~ dplyr::filter(., .data[[variable]] < max_threshold)
    ) %>%
    purrr::when(
      is.null(sd_threshold) ~ .,
      ~ dplyr::mutate(
        .,
        dplyr::across(.data[[variable]], ~ (mean(.x, na.rm = TRUE) +
                                              sd(.x, na.rm = TRUE) *
                                              sd_threshold),
                      .names = "{.col}_thr"
        ),
        dplyr::across(.data[[variable]], ~ ifelse(.x > (mean(.x, na.rm = TRUE) +
                                                          sd(.x, na.rm = TRUE) *
                                                          sd_threshold),
                                                  "discard", "keep"
        ),
        .names = "{.col}_thr_dec"
        )
      ) %>%
        dplyr::filter(dplyr::if_any(
          dplyr::ends_with("thr_dec"),
          ~ .x == "keep"
        )) %>%
        dplyr::select(-dplyr::ends_with("thr_dec"))
    ) %>%
    dplyr::ungroup()

  return(df_sacrois)
}

#' Fix values in the Maillage variable
#'
#' @param df_sacrois a sacrois data frame coming from sacrois_jour_flux_load.R
#'
#' @return a sacrois data frame with vessels variables
#' @export
#'
#' @examples df_sacrois <-  sacrois_jour_flux_load(sacrois_data_raw = sacrois_raw) %>%
#' sacrois_outliers_maillage(.)
sacrois_outliers_maillage <- function(df_sacrois) {
  df_sacrois <- df_sacrois %>%
    dplyr::mutate(MAILLAGE = dplyr::case_when(
      .data$MAILLAGE < 10 ~ .data$MAILLAGE * 10,
      .data$MAILLAGE > 300 ~ .data$MAILLAGE / 10,
      TRUE ~ .data$MAILLAGE
    ))
  return(df_sacrois)
}


#' Remove fishing sequences when outliers for a species variable in a sacrois data frame
#'
#' @param df_sacrois a sacrois data frame coming from sacrois_jour_flux_load.R
#' @param variable  which variable to filter from outliers
#' @param sd_threshold remove values that are above : mean(variable, na.rm = TRUE) + sd(variable, na.rm = TRUE) * sd_threshold)
#' @param grouping_variable choose which variable to use in a group_by
#'
#' @return a sacrois data frame with vessels variables
#' @export
#'
#' @examples  df_sacrois <-  sacrois_jour_flux_load(sacrois_data_raw = sacrois_raw) %>%
#' sacrois_set_origin(.) %>%
#' sacrois_outliers_species_variable(.)
sacrois_outliers_species_variable <- function(df_sacrois,
                                              variable = "weight",
                                              grouping_variable = NULL,
                                              sd_threshold = 3) {
  stopifnot(any(stringr::str_ends(string = names(df_sacrois), pattern = variable)))

  df_sacrois_seq <- df_sacrois %>%
    purrr::when(
      is.null(grouping_variable) ~ .,
      ~ dplyr::group_by(., across(all_of(grouping_variable)))
    ) %>%
    purrr::when(
      "ESP_COD_FAO" %in% names(df_sacrois)
      ~ dplyr::group_by(., .data$ESP_COD_FAO),
      ~.
    ) %>%
    dplyr::mutate(
      dplyr::across(dplyr::ends_with(variable), ~ (mean(.x, na.rm = TRUE) +
                                                     sd(.x, na.rm = TRUE) *
                                                     sd_threshold),
                    .names = "{.col}_thr"
      ),
      dplyr::across(dplyr::ends_with(variable), ~ ifelse(.x > (mean(.x, na.rm = TRUE) +
                                                                 sd(.x, na.rm = TRUE) *
                                                                 sd_threshold),
                                                         "discard", "keep"
      ),
      .names = "{.col}_thr_dec"
      )
    ) %>%
    dplyr::filter(dplyr::if_any(dplyr::ends_with("thr_dec"),
                                ~ .x == "discard")) %>%
    dplyr::ungroup() %>%
    dplyr::select("LE_ID_SEQ")

  df_sacrois <- dplyr::anti_join(df_sacrois,
                                 df_sacrois_seq,
                                 by = "LE_ID_SEQ") %>%
    dplyr::ungroup()

  return(df_sacrois)
}

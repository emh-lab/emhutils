#' # Define ggplot2 custom theme
#'
#' @param theme minimal as defaults but bw and classic are options
#' @param axis_text_x horizontal as default but diagonal or vertical
#' @param base_size default 10
#' @param legend_position right is the default, can be change to "none", "top", "bottom", "left"
#' @param font Helvetica as default
#' @param ... other parameters to be passed to ggplot2::theme()

#' @return a ggplot2 theme
#' @export
#'
#' @examples
#' \dontrun{
#' ggplot2_theme(
#'   theme = "minimal",
#'   axis_text_x = "horizontal"
#' )
#' }
ggplot2_theme <- function(
    theme = "minimal",
    base_size = 10,
    axis_text_x = "horizontal",
    legend_position = "right",
    font = "Helvetica", ...) {
  # Define valid options
  valid_axis_text_x <- c("horizontal", "vertical", "diagonal")
  valid_legend_positions <- c("none", "top", "bottom", "left", "right")

  # Check for valid axis_text_x input
  if (!axis_text_x %in% valid_axis_text_x) {
    stop(paste("Invalid axis_text_x value. Choose from:", paste(valid_axis_text_x, collapse = ", ")))
  }

  # Check for valid legend_position input
  if (!legend_position %in% valid_legend_positions) {
    stop(paste("Invalid legend_position value. Choose from:", paste(valid_legend_positions, collapse = ", ")))
  }

  # Base theme
  if (theme == "minimal") {
    theme_base <- ggplot2::theme_minimal(
      base_size = base_size
    )
  }

  if (theme == "bw") {
    theme_base <- ggplot2::theme_bw(
      base_size = base_size
    )
  }

  if (theme == "classic") {
    theme_base <- ggplot2::theme_classic(
      base_size = base_size
    )
  }
  theme_base <- theme_base +
    ggplot2::theme(
      strip.background = ggplot2::element_blank(),
      plot.background = ggplot2::element_blank(),
      # panel.grid.major = ggplot2::element_blank(),
      # panel.grid.minor = ggplot2::element_blank(),
      ...
    )

  # Adjust x-axis text orientation
  if (axis_text_x == "horizontal") {
    theme_base <- theme_base + ggplot2::theme(
      axis.text.x = ggplot2::element_text(angle = 0, vjust = 1, hjust = 0.5)
    )
  } else if (axis_text_x == "vertical") {
    theme_base <- theme_base + ggplot2::theme(
      axis.text.x = ggplot2::element_text(angle = 90, vjust = 0.5, hjust = 1)
    )
  } else if (axis_text_x == "diagonal") {
    theme_base <- theme_base + ggplot2::theme(
      axis.text.x = ggplot2::element_text(angle = 45, vjust = 1, hjust = 1)
    )
  }

  # Set legend position
  theme_base <- theme_base + ggplot2::theme(legend.position = legend_position)

  # Fonts
  theme_base <- theme_base + ggplot2::theme(
    text = ggplot2::element_text(family = font)
  )


  return(theme_base)
}

#' Plot annual average landings for a species by vessels
#'
#' @param df_sacrois a sacrois data frame
#' @param species_cod_fao a single species FAO code
#' @param species_threshold_weight a threshold in kg for plotting average annual landings per vessels
#'
#' @return a list with a summarize data frame, and 2 plots, one in kg and one in t
#' @export
#'
#' @examples sacrois_jour_flux_load(sacrois_data_raw = sacrois_raw) %>%
#' sacrois_set_origin(.) %>%
#' sacrois_plot_average_landings_navs(.,
#' species_cod_fao = "SOL",
#' species_threshold_weight = 1000)
sacrois_plot_average_landings_navs <- function(df_sacrois,
                                               species_cod_fao = "SOL",
                                               species_threshold_weight = 1000) {

  stopifnot(length(species_cod_fao) == 1,
            "weight" %in% names(df_sacrois),
            species_cod_fao %in% unique(df_sacrois$ESP_COD_FAO))

  list_avg <- list()

  list_avg$dfs_sacrois_average_navs <- df_sacrois %>%
    dplyr::filter(.data$ESP_COD_FAO == species_cod_fao) %>%
    dplyr::group_by(.data$NAVS_COD, .data$YEAR) %>%
    dplyr::summarise(weight = sum(.data$weight)) %>%
    dplyr::group_by(.data$NAVS_COD) %>%
    dplyr::summarise(mean_weight = mean(.data$weight),
                     mean_weight_t = .data$mean_weight / 1000)  %>%
    dplyr::mutate(dplyr::across(where(is.factor),
                                forcats::fct_drop),
                  dplyr::across(where(is.numeric),
                                round, digits = 0)) %>%
    dplyr::mutate(navs_threshold_weight = ifelse(species_threshold_weight <= .data$mean_weight,
                                                 "keep", "discard" ),
                  navs_threshold_weight_t = ifelse((species_threshold_weight / 1000) <= .data$mean_weight_t,
                                                   "keep", "discard" ))

  keep_perc_kg <- list_avg$dfs_sacrois_average_navs %>%
    dplyr::group_by(.data$navs_threshold_weight) %>%
    dplyr::summarise(n_navs_thr = dplyr::n()) %>%
    dplyr::ungroup() %>%
    dplyr::mutate(perc = round(.data$n_navs_thr / sum(.data$n_navs_thr) * 100, digits = 0)) %>%
    dplyr::filter(.data$navs_threshold_weight == "keep") %>%
    dplyr::pull()

  list_avg$plot_sacrois_average_navs <- ggplot2::ggplot() +
    ggplot2::geom_col(data = list_avg$dfs_sacrois_average_navs,
                      ggplot2::aes(x = forcats::fct_reorder(.data$NAVS_COD,
                                                            .data$mean_weight),
                                   y = .data$mean_weight,
                                   fill = .data$navs_threshold_weight)) +
    ggplot2::xlab("Vessels") +
    ggplot2::ylab("Average annual landings (in kg)") +
    ggplot2::theme(axis.text.x =  ggplot2::element_blank()) +
    ggplot2::labs(fill = "") +
    ggplot2::ggtitle(glue::glue("Percentage of kept vessels : {keep_perc_kg}%")) +
    ggplot2::scale_fill_manual(values = c("#838B8B", "#104E8B"))

  keep_perc_t <- list_avg$dfs_sacrois_average_navs %>%
    dplyr::group_by(.data$navs_threshold_weight_t) %>%
    dplyr::summarise(n_navs_thr = dplyr::n()) %>%
    dplyr::ungroup() %>%
    dplyr::mutate(perc = round(.data$n_navs_thr / sum(.data$n_navs_thr) * 100,
                               digits = 0)) %>%
    dplyr::filter(.data$navs_threshold_weight_t == "keep") %>%
    dplyr::pull()

  list_avg$plot_sacrois_average_navs_t <- ggplot2::ggplot() +
    ggplot2::geom_col(data = list_avg$dfs_sacrois_average_navs,
                      ggplot2::aes(y = forcats::fct_reorder(.data$NAVS_COD,
                                                            .data$mean_weight_t),
                                   x = .data$mean_weight_t,
                                   fill = .data$navs_threshold_weight_t)) +
    ggplot2::ylab("Vessels") +
    ggplot2::xlab("Average annual landings (in t)") +
    ggplot2::theme(axis.text.y =  ggplot2::element_blank())  +
    ggplot2::labs(fill = "") +
    ggplot2::ggtitle(glue::glue("Percentage of kept vessels : {keep_perc_t}%")) +
    ggplot2::scale_fill_manual(values = c("#838B8B", "#104E8B")) +
    ggplot2::geom_vline(xintercept = 1, linetype = 'dashed') +
    ggplot2::annotate("text", y =  40, x = 0.5, label = "1t") +
    ggplot2::geom_vline(xintercept = 5, linetype = 'dashed') +
    ggplot2::annotate("text", y =  40, x = 5.5, label = "5t") +
    ggplot2::geom_vline(xintercept = species_threshold_weight/1000, linetype = 'dashed') +
    ggplot2::annotate("text", y =  40, x = (species_threshold_weight/1000 + 0.5),
                      label = glue::glue("{species_threshold_weight/1000}t")) +
    ggplot2::geom_vline(xintercept = 10, linetype = 'dashed') +
    ggplot2::annotate("text", y =  40, x = 11, label = "10t")




  list_avg$plot_sacrois_average_navs_1t <- ggplot2::ggplot() +
    ggplot2::geom_col(data = dplyr::filter(list_avg$dfs_sacrois_average_navs,
                                           .data$mean_weight_t >= 1),
                      ggplot2::aes(y = forcats::fct_reorder(.data$NAVS_COD,
                                                            .data$mean_weight_t),
                                   x = .data$mean_weight_t,
                                   fill = .data$navs_threshold_weight_t)) +
    ggplot2::ylab("Vessels") +
    ggplot2::xlab("Average annual landings (in t)") +
    ggplot2::theme(axis.text.y =  ggplot2::element_blank())  +
    ggplot2::labs(fill = "") +
    ggplot2::ggtitle(glue::glue("Percentage of kept vessels : {keep_perc_t}%")) +
    ggplot2::scale_fill_manual(values = c("#838B8B", "#104E8B")) +
    ggplot2::geom_vline(xintercept = 1, linetype = 'dashed') +
    ggplot2::annotate("text", y =  40, x = 0.5, label = "1t") +
    ggplot2::geom_vline(xintercept = 5, linetype = 'dashed') +
    ggplot2::annotate("text", y =  40, x = 5.5, label = "5t") +
    ggplot2::geom_vline(xintercept = species_threshold_weight/1000, linetype = 'dashed') +
    ggplot2::annotate("text", y =  40, x = (species_threshold_weight/1000 + 0.5),
                      label = glue::glue("{species_threshold_weight/1000}t")) +
    ggplot2::geom_vline(xintercept = 10, linetype = 'dashed') +
    ggplot2::annotate("text", y =  40, x = 11, label = "10t")

  list_avg$plot_sacrois_average_navs_t_hist <- ggplot2::ggplot() +
    ggplot2::geom_histogram(data = list_avg$dfs_sacrois_average_navs,
                            ggplot2::aes(x = .data$mean_weight_t,
                                         fill = .data$navs_threshold_weight_t),
                            binwidth = 1,
                            boundary = 0, closed = "left") +
    ggplot2::ylab("Number of vessels") +
    ggplot2::xlab("Average annual landings (in t)") +
    ggplot2::labs(fill = "") +
    ggplot2::scale_fill_manual(values = c("#838B8B", "#104E8B")) +
    ggplot2::geom_vline(xintercept = 1, linetype = 'dashed') +
    ggplot2::annotate("text", y =  400, x = 0.5, label = "1t") +
    ggplot2::geom_vline(xintercept = 5, linetype = 'dashed') +
    ggplot2::annotate("text", y =  400, x = 5.5, label = "5t") +
    ggplot2::geom_vline(xintercept = species_threshold_weight/1000, linetype = 'dashed') +
    ggplot2::annotate("text", y =  400, x = (species_threshold_weight/1000 + 0.5),
                      label = glue::glue("{species_threshold_weight/1000}t")) +
    ggplot2::geom_vline(xintercept = 10, linetype = 'dashed') +
    ggplot2::annotate("text", y =  400, x = 11, label = "10t")


  list_avg$plot_sacrois_average_navs_1t_hist <- ggplot2::ggplot() +
    ggplot2::geom_histogram(data = dplyr::filter(list_avg$dfs_sacrois_average_navs,
                                                 .data$mean_weight_t >= 1),
                            ggplot2::aes(x = .data$mean_weight_t,
                                         fill = .data$navs_threshold_weight_t),
                            binwidth = 1,
                            boundary = 0, closed = "left") +
    ggplot2::ylab("Number of vessels") +
    ggplot2::xlab("Average annual landings (in t)") +
    ggplot2::labs(fill = "") +
    ggplot2::scale_fill_manual(values = c("#838B8B", "#104E8B")) +
    ggplot2::geom_vline(xintercept = 1, linetype = 'dashed') +
    ggplot2::annotate("text", y =  400, x = 0.5, label = "1t") +
    ggplot2::geom_vline(xintercept = 5, linetype = 'dashed') +
    ggplot2::annotate("text", y =  400, x = 5.5, label = "5t") +
    ggplot2::geom_vline(xintercept = species_threshold_weight/1000, linetype = 'dashed') +
    ggplot2::annotate("text", y =  400, x = (species_threshold_weight/1000 + 0.5),
                      label = glue::glue("{species_threshold_weight/1000}t")) +
    ggplot2::geom_vline(xintercept = 10, linetype = 'dashed') +
    ggplot2::annotate("text", y =  400, x = 11, label = "10t")



  return(list_avg)

}

#' Load sacrois Rdata files
#'
#' @param path_data_flux_sacrois path of the datafile as a string
#' @param sacrois_data_raw a sacrois dataset with the format "NAVIRES_MOIS_MAREES_JOUR_IFR"
#' @param path_data_sacrois  path to save data as a string
#' @param year year to load as numeric value
#' @param filter_na if TRUE remove all NA or missing values
#' @param sacrois_dataset choose which dataset to load default NAVIRES_MOIS_MAREES_JOUR_IFR
#' @param save_df if true save dataset as rds files before and after filter_na if set to TRUE
#' @param erase_rds if true erase previously saved rds files, if not rds files is loaded from previous run
#' @param zone_SACROIS_NIV3 choose which zone to work with default is "27.8.a", "27.8.b"
#'
#' @return a sacrois data frame
#' @export
#'
#' @examples
#' sacrois_jour_flux_load(sacrois_data_raw = sacrois_raw)
sacrois_jour_flux_load <- function(path_data_flux_sacrois = NULL,
                                   sacrois_data_raw = NULL,
                                   path_data_sacrois = NULL,
                                   year = NULL,
                                   filter_na = FALSE,
                                   sacrois_dataset = "NAVIRES_MOIS_MAREES_JOUR_IFR",
                                   save_df = FALSE,
                                   erase_rds = TRUE,
                                   zone_SACROIS_NIV3 = c("27.8.a", "27.8.b")) {

  stopifnot((!is.null(path_data_flux_sacrois) & is.null(sacrois_data_raw)) |
              (is.null(path_data_flux_sacrois) & !is.null(sacrois_data_raw)))

  ### ---------------------------------------------------------------------------
  ### check if file exists in case when erase_rds == FALSE
  if ((erase_rds == FALSE & filter_na == FALSE &
       file.exists(paste0(
         path_data_sacrois,
         "/df_sacrois_raw_", year, ".rds"
       ))) |

      (erase_rds == FALSE & filter_na == TRUE &
       file.exists(paste0(
         path_data_sacrois,
         "/df_sacrois_raw_no_na_", year, ".rds"
       )))) {
    if (erase_rds == FALSE & filter_na == FALSE &
        file.exists(paste0(
          path_data_sacrois,
          "/df_sacrois_raw_", year, ".rds"
        ))) {
      df_sacrois <- readr::read_rds(file = paste0(
        path_data_sacrois,
        "/df_sacrois_raw_", year, ".rds"
      ))
    }

    if (erase_rds == FALSE & filter_na == TRUE &
        file.exists(paste0(
          path_data_sacrois,
          "/df_sacrois_raw_no_na_", year, ".rds"
        ))) {
      df_sacrois <- readr::read_rds(file = paste0(
        path_data_sacrois,
        "/df_sacrois_raw_no_na_", year, ".rds"
      ))
    }
  } else {

    if (!is.null(path_data_flux_sacrois) & is.null(sacrois_data_raw)) {
      ### ---------------------------------------------------------------------------
      ### extract the right file name
      sacrois_data_file <- intersect(
        list.files(
          path = paste0(path_data_flux_sacrois),
          pattern = "SACROIS"
        ),
        list.files(
          path = paste0(path_data_flux_sacrois),
          pattern = as.character(year)
        )
      )

      ### load file
      load(paste0(path_data_flux_sacrois, "/", sacrois_data_file))

      ### as dataframe
      df_sacrois <- get(paste0(sacrois_dataset, "_", year)) %>%
        dplyr::mutate(sacrois_version = stringr::str_match(sacrois_data_file, "v\\s*(.*?)\\s*.RData")[, 2])
    }

    if (is.null(path_data_flux_sacrois) & !is.null(sacrois_data_raw)) {
      df_sacrois <- sacrois_data_raw
    }

    df_sacrois <- df_sacrois %>%
      tibble::as_tibble() %>%
      dplyr::filter(
        # Que les peches dans le golfe de Gascogne
        .data$SECT_COD_SACROIS_NIV3 %in% zone_SACROIS_NIV3
      ) %>%
      dplyr::mutate(
        NAVS_COD = factor(.data$NAVS_COD),
        PAVILLON = factor(.data$PAVILLON),
        MAREE_ID = factor(.data$MAREE_ID),
        MAREE_DATE_DEP = lubridate::dmy_hms(.data$MAREE_DATE_DEP),
        MAREE_DATE_RET = lubridate::dmy_hms(.data$MAREE_DATE_RET),
        QUANT_POIDS_VIF_SACROIS = as.numeric(.data$QUANT_POIDS_VIF_SACROIS),
        QUANT_POIDS_VIF_SIPA = as.numeric(.data$QUANT_POIDS_VIF_SIPA),
        ORIGINE_QUANT_POIDS_VIF = factor(.data$ORIGINE_QUANT_POIDS_VIF),
        TPS_MER = as.numeric(.data$TPS_MER),
        TP_NAVIRE_SACROIS = as.numeric(.data$TP_NAVIRE_SACROIS),
        TP_NAVIRE_SIPA = as.numeric(.data$TP_NAVIRE_SIPA),
        LIEU_COD_DEP_SACAPT = factor(.data$LIEU_COD_DEP_SACAPT),
        LIEU_COD_RET_SACAPT = factor(.data$LIEU_COD_RET_SACAPT),
        LIEU_COD_DEP_SACROIS = factor(.data$LIEU_COD_DEP_SACROIS),
        LIEU_COD_RET_SACROIS = factor(.data$LIEU_COD_RET_SACROIS),
        SEQ_ID = factor(.data$SEQ_ID),
        DATE_SEQ = lubridate::dmy_hms(.data$DATE_SEQ),
        DATE_SEQ_ORIGINE = factor(.data$DATE_SEQ_ORIGINE),
        METIER_COD_SACROIS = factor(.data$METIER_COD_SACROIS),
        METIER_DCF_6_COD = factor(.data$METIER_DCF_6_COD),
        ENGIN_COD = factor(.data$ENGIN_COD),
        ENGIN_ORIGINE = factor(.data$ENGIN_ORIGINE),
        SECT_COD_SACROIS_NIV1 = factor(.data$SECT_COD_SACROIS_NIV1),
        SECT_COD_SACROIS_NIV3 = factor(.data$SECT_COD_SACROIS_NIV3),
        SECT_COD_SACROIS_NIV5 = factor(.data$SECT_COD_SACROIS_NIV5),
        SECT_COD_ORIGINE = factor(.data$SECT_COD_ORIGINE),
        DIMENSION = as.character(.data$DIMENSION),
        # On garde la variable DIMENSION comme une var texte pour l'instant
        # Elle sera transformee en var quanti dans un script ulterieur
        MAILLAGE = as.numeric(.data$MAILLAGE),
        MAILLAGE_ORIGINE = factor(.data$MAILLAGE_ORIGINE),
        YEAR = factor(lubridate::year(.data$MAREE_DATE_RET)),
        MONTH = factor(lubridate::month(.data$MAREE_DATE_RET)),
        QUARTER = factor(lubridate::quarter(.data$MAREE_DATE_RET)),
        dplyr::across(where(is.factor), forcats::fct_drop)
      )

    if (save_df) {
      if (is.null(year)) {
        year <- unique(df_sacrois$YEAR) %>%
          as.character()
      }
      saveRDS(df_sacrois,
              file = paste0(
                path_data_sacrois,
                "/df_sacrois_raw_", year, ".rds"
              )
      )
    }

    ### -------------------------------------------------------------------------
    ### remove NA values
    if (filter_na) {
      df_sacrois <- df_sacrois %>%
        dplyr::filter(
          .data$NAVS_COD != "", !is.na(.data$NAVS_COD),
          .data$MAREE_ID != "", !is.na(.data$MAREE_ID),
          !is.na(.data$MAREE_DATE_DEP),
          !is.na(.data$MAREE_DATE_RET),
          .data$LIEU_COD_DEP_SACAPT != "", !is.na(.data$LIEU_COD_DEP_SACAPT),
          .data$LIEU_COD_RET_SACAPT != "", !is.na(.data$LIEU_COD_RET_SACAPT),
          .data$LIEU_COD_DEP_SACROIS != "", !is.na(.data$LIEU_COD_DEP_SACROIS),
          .data$LIEU_COD_RET_SACROIS != "", !is.na(.data$LIEU_COD_RET_SACROIS),
          .data$SEQ_ID != "", !is.na(.data$SEQ_ID),
          .data$METIER_COD_SACROIS != "", !is.na(.data$METIER_COD_SACROIS),
          .data$METIER_DCF_5_COD != "", !is.na(.data$METIER_DCF_5_COD),
          .data$METIER_DCF_6_COD != "", !is.na(.data$METIER_DCF_6_COD),
          .data$ENGIN_COD != "", !is.na(.data$ENGIN_COD),
          .data$ENGIN_ORIGINE != "", !is.na(.data$ENGIN_ORIGINE),
          .data$SECT_COD_ORIGINE != "", !is.na(.data$SECT_COD_ORIGINE),
          .data$SECT_COD_SACROIS_NIV5 != "", !is.na(.data$SECT_COD_SACROIS_NIV5),
          .data$ESP_COD_FAO != "", !is.na(.data$ESP_COD_FAO)
        ) %>%
        dplyr::mutate(dplyr::across(where(is.factor), forcats::fct_drop)) %>%
        dplyr::distinct()

      if (save_df) {
        saveRDS(df_sacrois,
                file = paste0(paste0(
                  path_data_sacrois,
                  "/df_sacrois_raw_no_na_", year, ".rds"
                ))
        )
      }
    }
  }
  return(df_sacrois)
}

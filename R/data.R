#' Sacrois data set
#'
#' A dataset containing sacrois 2020 data sampled and anonymized
#'
#' @format A data frame with 100 rows and 64 variables:
#' \describe{
#' }
"sacrois_raw"

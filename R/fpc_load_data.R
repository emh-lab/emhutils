#' Load FPC data
#'
#' @param path_to_fpc_file a string with the path of the data
#' @param fpc_file a string with the file name
#'
#' @return a data frame of the FPC file
#' @export
#'
#' @examples \dontrun{fpc_load_data(path_to_fpc_file = "data_base/ref_navs",
#'  "2000-ISIH-19926-vueAnnuelleFpc-20180208114307")  }
fpc_load_data <- function(path_to_fpc_file,
                          fpc_file) {

  df_navire <- utils::read.csv2(glue::glue("{path_to_fpc_file}/{fpc_file}"),
                         header = TRUE,
                         fileEncoding = "ISO-8859-1",
                         sep = ";") %>%
    dplyr::mutate(fpc_year = factor(substr(fpc_file, 1, 4)),
                  NAVS_COD = factor(.data$NAVS_COD),
                  dplyr::across(where(is.character), as.factor))

  return(df_navire)
}

#' Merge monkfish in one species
#'
#' @param df_sacrois a sacrois data frame
#'
#' @return a sacrois data frame with MNZ as ESP_COD_FAO for all monkfish
#' @export
#'
#' @examples df_sacrois <-  sacrois_jour_flux_load(sacrois_data_raw = sacrois_raw) %>%
#' sacrois_merge_monkfish(.)
sacrois_merge_monkfish <- function(df_sacrois){

  df_sacrois <- df_sacrois %>%
    dplyr::mutate(ESP_COD_FAO = forcats::fct_recode(.data$ESP_COD_FAO,
                                             MNZ = "ANK",
                                             MNZ = "MON"))
  return(df_sacrois)
}

#' Standardize numeric variable
#'
#' @param x a numeric vector
#'
#' @return a standardize x
#' @export
#'
#' @examples
#' standardize_var(runif(100))
standardize_var <- function(x) {
  stopifnot(is.numeric(x))
  (x - mean(x, na.rm = TRUE)) / stats::sd(x, na.rm = TRUE)
  x
}

# emhUtils

<!-- badges: start -->
<!-- badges: end -->

The goal of emhUtils is to provide functions to handle and manipulate data.

## Installation

You can install the development version of emhUtils like using the remotes package

``` r
install.packages("remotes")
library(remotes)
remotes::install_gitlab(repo = "emh-lab/emhutils",
                        host = "https://gitlab.ifremer.fr")
```

## Example

This is a basic example which shows you how to solve a common problem:

``` r
library(emhUtils)
x <- c(1,2,1,3,4,5,1,5,2,6,3,4)
length_unique(x)
```


